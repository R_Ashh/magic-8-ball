//
//  ViewController.swift
//  Magic 8-Ball
//
//  Created by Arash Afshar on 8/18/18.
//  Copyright © 2018 Arash Afshar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var randomNumber : Int = 0
    let imageNames = ["ball1", "ball2", "ball3", "ball4", "ball5"]
    
    @IBOutlet weak var ballImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        randomBallGenerator()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func buttonForRoll(_ sender: UIButton) {
        randomBallGenerator()
    }
    
    func randomBallGenerator(){
        randomNumber = Int(arc4random_uniform(5))
        ballImage.image = UIImage.init(named: imageNames[randomNumber])
    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        randomBallGenerator()
    }
    
}

